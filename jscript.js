function validateForm() {

    let nameRGEX = /^\S/;
    let phoneRGEX = /^\d/;
    let emailRGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    let fname = document.orderForm.fname.value;
    let fnameResult = nameRGEX.test(fname);
    if (fname === "") {
        alert("First name is obligatory");
        return false;
    } else if (fnameResult === false) {
        alert("Only letters allowed in first name")
        return false;
    }

    let lname = document.orderForm.lname.value;
    let lnameResult = nameRGEX.test(lname);
    if (lname === "") {
        alert("Last name is obligatory");
        return false;
    } else if (lnameResult === false) {
        alert("Only letters allowed in last name")
        return false;
    }

    let phone = document.orderForm.phone.value;
    if (phone !== "") {
        let phoneResult = phoneRGEX.test(phone);
        if (phoneResult === false) {
            alert("Only numbers are allowed");
            return false;
        }
        if (phone.length < 9) {
            alert("Minimum 9 numbers required");
            return false;
        }
    } else if (phone === "") {
        alert("Phone number is obligatory");
        return false;
    }

    let address = document.orderForm.address.value;
    if (address === "") {
        alert("Address is obligatory");
        return false;
    }

    let hnumber = document.orderForm.hnumber.value;
    if (hnumber === "") {
        alert("House number is obligatory");
        return false;
    }


    let cname = document.orderForm.cname.value;
    if (cname === "") {
        alert("City is obligatory");
        return false;
    }

    let email = document.orderForm.email.value;
    let emailResult = emailRGEX.test(email);
    if (email === "") {
        alert("Email is obligatory");
        return false;
    } else if (emailResult === false) {
        alert("Please enter correct email");
        return false;
    }

    let message = document.orderForm.message.value;
    if (message === "") {
        alert("Message is obligatory");
        return false;
    }

    const formElements = Array.from(document.orderForm.elements);

    formElements.forEach(element => {
        console.log(element.value);
    });
    return false;
}